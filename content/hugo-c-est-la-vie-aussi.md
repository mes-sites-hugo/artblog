+++
categories = ["Web"]
date = 2020-07-07T17:19:00Z
description = "Hugo mister générateur"
image = "./uploads/hugo.jpg"
tags = ["Hébergement", "CMS"]
title = "Hugo, c'est la vie ! (aussi)"
type = "post"

+++
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis urna risus. Nunc mollis sit amet felis at eleifend. In fringilla ipsum ipsum, vel maximus magna tempus eu. Maecenas sit amet ex sed dolor consequat consectetur. Morbi libero neque, fermentum pellentesque porta at, rutrum vitae odio. Praesent eu auctor diam. Nam quis est malesuada, aliquet elit eu, varius ligula.

Maecenas blandit massa eget lectus sodales, in laoreet lorem cursus. Quisque egestas condimentum enim non euismod. Proin dapibus vitae sapien at rhoncus. Proin sed ante elit. Maecenas pretium aliquam nisi, ac luctus mauris volutpat id. Pellentesque sit amet ipsum urna. Cras imperdiet orci augue, a aliquam purus finibus feugiat. Fusce sed placerat metus, eu condimentum diam. Sed mollis sollicitudin mauris, quis commodo lorem mattis ut. Donec nec est egestas, pulvinar ipsum a, placerat velit. Nunc vitae diam dictum, ullamcorper odio ut, sagittis libero. Morbi quis elit fermentum, condimentum urna ac, commodo eros. Donec ut tincidunt urna, sed ultrices tellus. Nam fringilla urna maximus nisl euismod, eget pharetra ipsum eleifend. Fusce vestibulum sodales tincidunt. Sed sapien diam, lobortis non erat vitae, sagittis blandit dolor.

Vivamus non nunc elit. Morbi ultricies augue et luctus tristique. Praesent id enim porttitor, consequat nulla ut, cursus leo. Duis condimentum metus eget magna ultricies, eget laoreet mauris dignissim. Nullam vitae vestibulum ligula. Etiam aliquam, lacus vel scelerisque pellentesque, mi odio malesuada lectus, ac egestas arcu orci sit amet arcu. Nam sed nunc eget lorem placerat laoreet et id quam.

![](./uploads/hugo.jpg)

C'est relativement stylé !