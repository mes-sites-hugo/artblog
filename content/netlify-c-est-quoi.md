+++
categories = ["Hébergement"]
date = 2020-07-07T17:04:00Z
description = "Netlify, tour d'inspection"
image = "./uploads/logo_netlify.jpg"
tags = ["Hébergement"]
title = "Netlify c'est quoi ?"
type = "featured"

+++
## Pour Macg.co

> Pour créer un site, on peut toujours partir de pages blanches (virtuelles) et écrire du code en HTML, CSS et JavaScript à partir de zéro. C’est l’approche traditionnelle et même si elle n’a pas perdu complètement de son intérêt, elle est devenue extrêmement minoritaire aujourd'hui. On utilise en général un [**CMS**](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu), une application qui permet de gérer et d’organiser du contenu.
>
> Dans le domaine, les CMS dynamiques ont la cote, WordPress en tête. Construits autour d’une base de données, ils génèrent le site à la volée, en créant des pages à partir de modèles et d’informations piochées dans la base. Cette approche a de nombreux avantages, le premier étant la souplesse — il suffit de changer un paramètre du thème pour changer tout le site —, mais elle a aussi des inconvénients, notamment en termes de performances ou de sécurité.